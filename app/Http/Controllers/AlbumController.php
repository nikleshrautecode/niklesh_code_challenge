<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Album;
use App\Models\Photo;

class AlbumController extends Controller
{

    public function albums (Request $request) {
    	$requestData = $request->all();
    	$responseData = array();
    	if($requestData['str'] ?? false){
    		$str = $requestData['str'];
    		$responseData['albums'] = Album::where('album_title', 'LIKE', "%$str%")->get();
    		$responseData['photos'] = Photo::where('photo_title', 'LIKE', "%$str%")->get();
    	}else{
    		$responseData['albums'] = Album::all();
    		$responseData['photos'] = Photo::all();
    	}
    	return $responseData;
    }

    public function album ($id=null) {
    	$responseData['albums'] = [];
    	$responseData['photos'] = [];
    	if($id ?? false){
    		$responseData['album'] = Album::find($id);
    		$responseData['photos'] = Photo::where('album_id', $id)->get();
    	}
    	return $responseData;	
    }
}
