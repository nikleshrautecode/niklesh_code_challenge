<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Photo;

class PhotoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Photo::truncate();
        $insertData = [
        	['photo_title' => 'Photo 1', 'album_id' => 1,'url'=>'1.jpg'],
        	['photo_title' => 'Photo 2', 'album_id' => 1,'url'=>'2.jpg'],
        	['photo_title' => 'Photo 3', 'album_id' => 2,'url'=>'3.jpg'],
        	['photo_title' => 'Photo 4', 'album_id' => 2,'url'=>'4.jpg'],
        	['photo_title' => 'Photo 5', 'album_id' => 3,'url'=>'5.jpg'],
        	['photo_title' => 'Photo 6', 'album_id' => 3,'url'=>'6.jpg'],
        ];
        Photo::insert($insertData);
    }
}
