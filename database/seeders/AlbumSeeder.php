<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Album;

class AlbumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Album::truncate();
        $insertData = [
        	['album_title' => 'Album 1'],
        	['album_title' => 'Test 2'],
        	['album_title' => 'Department 3']
        ];
        Album::insert($insertData);
    }
}
